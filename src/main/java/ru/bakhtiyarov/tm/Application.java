package ru.bakhtiyarov.tm;

import ru.bakhtiyarov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String... args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static boolean parseArgs(final String... args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                displayAbout();
                break;
            case TerminalConst.HELP:
                displayHelp();
                break;
            case TerminalConst.VERSION:
                displayVersion();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                break;
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruslan Bakhtiyarov");
        System.out.println("E-MAIL: rusya.vay@mail.ru");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.ABOUT + " - Show developer info.");
        System.out.println(TerminalConst.VERSION + " - Show developer info.");
        System.out.println(TerminalConst.HELP + " - Display terminal commands.");
        System.out.println(TerminalConst.EXIT + " - Close application.");
    }

    private static void exit() {
        System.exit(0);
    }

}

